<?php

declare(strict_types=1);

namespace App\Service;


use App\Strategies\FileParserInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BookFileParser
{

    private FileParserInterface $strategy;

    public function __construct(FileParserInterface $strategy) {
        $this->strategy = $strategy;
    }


    public function parseUploadedFile(UploadedFile $file) : array
    {
        return $this->strategy->parseFile($file);
    }
}
