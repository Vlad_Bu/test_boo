<?php

declare(strict_types=1);

namespace App\Strategies;

use App\Entity\Book;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

class YamlFileParser implements FileParserInterface
{
    public function parseFile(UploadedFile $file) : array
    {
        $entities = [];

        foreach ( Yaml::parseFile($file->getRealPath()) as $book) {
             $entities[] = Book::create(
                 $book['title'],
                 $book['description'],
                 $book['author'],
             );
        }
       return $entities;
    }

}
