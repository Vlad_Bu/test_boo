<?php

declare(strict_types=1);

namespace App\Strategies;

use App\Entity\Book;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CsvFileParser implements FileParserInterface
{

    public function parseFile(UploadedFile $file) : array
    {
        $entities = [];

        if (($handle = fopen($file->getRealPath(), "r")) !== false) {
            $i = 0;
            while (($data = fgetcsv($handle, null, ";")) !== false) {
                $i++;
                $entities[] = Book::create(...str_getcsv($data[0]));
            }
            fclose($handle);
        }

        return $entities;
    }
}
