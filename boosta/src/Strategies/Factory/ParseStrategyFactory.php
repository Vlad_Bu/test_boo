<?php

declare(strict_types=1);

namespace App\Strategies\Factory;

use App\Form\BookType;
use App\Strategies\CsvFileParser;
use App\Strategies\FileParserInterface;
use App\Strategies\JsonFileParser;
use App\Strategies\YamlFileParser;

class ParseStrategyFactory
{
    public static function create(string $mimeType) : FileParserInterface
    {
        switch ($mimeType) {
            case in_array($mimeType, BookType::CSV_MIME_TYPES):
                return new CsvFileParser();
                break;
            case in_array($mimeType, BookType::JSON_MIME_TYPES):
                return new JsonFileParser();
                break;
            case in_array($mimeType, BookType::YAML_MIME_TYPES):
                return new YamlFileParser();
                break;
        }

    }

}
