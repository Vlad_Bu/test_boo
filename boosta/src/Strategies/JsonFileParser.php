<?php

declare(strict_types=1);

namespace App\Strategies;

use App\Entity\Book;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class JsonFileParser implements FileParserInterface
{
    public function parseFile(UploadedFile $file) : array
    {
        $entities = [];
        foreach (json_decode(file_get_contents($file->getRealPath())) as $book)
        {
            $entities[] = Book::create(
                $book->title,
                $book->description,
                $book->author,
            );
        }
        return $entities;
    }
}
