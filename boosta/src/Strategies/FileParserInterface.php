<?php

declare(strict_types=1);

namespace App\Strategies;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileParserInterface
{
    public function parseFile( UploadedFile $file): array;
}
