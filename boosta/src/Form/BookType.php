<?php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    const CSV_MIME_TYPES = [
        'text/csv',
        'text/x-comma-separated-values',
        'text/x-csv',
        'application/csv',
        'text/plain',
    ];

    const JSON_MIME_TYPES = ['application/json'];
    const YAML_MIME_TYPES = [
        'application/x-yaml',
        'text/x-yaml',
        'text/yaml',
        'application/x-yaml',
        'text/x-yaml',
        'text/yaml',
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title',TextType::class,[ 'required' => true])
                ->add('description',TextType::class,[ 'required' => true])
                ->add('author',TextType::class,[ 'required' => true])
            ->add(
                'file',
                FileType::class,
                [
                    'required'    => false,
                    "constraints" => [
                        new File(
                            [
                                "maxSize"   => "100M",
                                "mimeTypes" => [
                                    ...self::CSV_MIME_TYPES,
                                    ...self::JSON_MIME_TYPES,
                                    ...self::YAML_MIME_TYPES,
                                ],

                            ]
                        ),
                    ],
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
