<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Book;
use App\Service\BookFileParser;
use App\Strategies\Factory\ParseStrategyFactory;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use App\Form\BookType;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class BookController extends AbstractController
{
    /**
     * @Route("/", name="book_index", methods={"GET"})
     */
    public function index(BookRepository $bookRepository) : Response
    {
        return $this->render(
            'book/index.html.twig',
            [
                'books' => $bookRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/new", name="book_new", methods={"GET","POST"})
     */
    public function new(Request $request) : Response
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $books = [];
            if ($file = $form->get('file')->getData()) {

                $strategy = ParseStrategyFactory::create($file->getClientMimeType());
                $parser   = new BookFileParser($strategy);
                $books    = $parser->parseUploadedFile($file);
            }
            $title       = $form->get('title')->getNormData();
            $description = $form->get('description')->getNormData();
            $author      = $form->get('author')->getNormData();
            if ($title && $description && $author) {
                $books[] = Book::create(
                    $form->get('title')->getNormData(),
                    $form->get('description')->getNormData(),
                    $form->get('author')->getNormData()
                );
            }
            foreach ($books as $book) {
                $entityManager->persist($book);
            }

            try {
                $entityManager->flush();
            }
            catch (UniqueConstraintViolationException $exception) {
                $this->addFlash('error', 'Some books is already exist in DB! Try again. ');
            }

            return $this->redirectToRoute('book_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render(
            'book/new.html.twig',
            [
                'book' => $book,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id}", name="book_show", methods={"GET"})
     */
    public function show(Book $book) : Response
    {
        return $this->render(
            'book/show.html.twig',
            [
                'book' => $book,
            ]
        );
    }
}
